(function($, document) {
	$(document).ready(function() {
		$('.testimonial-slider').owlCarousel({
			dots: true,
			items: 1,
			center: true,
			lazyLoad: true,
			nav: true,
			navText: [
				'<svg><use xlink:href="#arrow-left"></use></svg>',
				'<svg><use xlink:href="#arrow-right"></use></svg>'
			]
		});

		$('.slider').owlCarousel({
			items: 1,
			dots: false,
			center: true,
			lazyLoad: true,
			nav: true,
			navText: [
				'<svg><use xlink:href="#arrow-left"></use></svg>',
				'<svg><use xlink:href="#arrow-right"></use></svg>'
			]
		});

		heroHeight();
		makefixed($(window).scrollTop());
	});

	$('.nav-menu a').click(function() {
		var href = $(this).attr('href');
		var h = $('.nav').height();
		$('html, body').stop().animate({ scrollTop: $(href).offset().top - h }, 500);
		return false;
	});

	$('#menu-burger').click(function(){
		var $nav = $('.nav');
		var openClass = 'menu-open';
		$nav.toggleClass(openClass);
	})

	$(window).resize(function() {
		var $nav = $('.nav');
		var openClass = 'menu-open';
		$nav.removeClass(openClass);

		heroHeight();

	});

	$(window).scroll(function() {
		makefixed($(window).scrollTop());
	});

	function makefixed(pos) {
		var $hero = $('.hero');
		var $nav = $('.nav');
		var fixedClass = "fixed";
		var openClass = 'menu-open';
		if (pos > $hero.height() - $nav.height()) {
			$nav.addClass(fixedClass);
		} else {
			$nav.removeClass(fixedClass);
		}
	}

	function heroHeight() {
		var h = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;
		var w = $(window).width()
		var $hero = $('.hero-wrap');

		if (w > 992) {
			$hero.height(h);
		} else {
			$hero.removeAttr('style');
		}
	}

})(jQuery, document);
